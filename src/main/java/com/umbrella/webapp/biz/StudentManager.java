package com.umbrella.webapp.biz;

import com.umbrella.webapp.entity.Student;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author d.traverso
 */
@Service
public class StudentManager {
    
    @Autowired
    private EntityManager em;

    public StudentManager() {
    }

    public Student getDefaultStudent() {
        Student defaultStudent = new Student();
        defaultStudent.setName("Default Student");
        return defaultStudent;
    }

    public Student getStudent(Long id) {
        return em.find(Student.class, id);
    }

    public void createStudent(Student student) {
        em.persist(student);
    }    
}