package com.umbrella.webapp.ws;

import com.umbrella.webapp.biz.StudentManager;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.springframework.stereotype.Service;

/**
 *
 * @author d.traverso
 */
@Service("students")
@Path("students")
public class Students {
    
    public Students(){}
    
    @Inject
    StudentManager studentManager;
    
    @GET
    public String getDefaultStudent(){
        return studentManager.getDefaultStudent().toString();
    }    
}