/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.umbrella.webapp.entity;

/**
 *
 * @author a.just
 *
 */
public interface IBasePersistentObject {

    /**
     *
     * @return the synthetic key
     */
    public Long getId();

    /**
     *
     * @param id The synthetic key
     */
    public void setId(Long id);

}
